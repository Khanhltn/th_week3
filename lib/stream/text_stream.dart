import 'dart:async';

class TextStream {

  Stream<String> getText() async* {
    final List<String> texts = [ "Hí anh em, lại là mình đây",
      "Kanes team here we go", "Bớt khùm lại ạ", "cái gì vậy trời", "u là trời", "Thật ngu ngok!!"];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 5;
      return texts[index];
    });
  }
}
